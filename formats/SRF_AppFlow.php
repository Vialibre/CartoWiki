<?php

/**
 * SRF Printer for graphing flows between applications
 *
 * @file SRF_AppFlow.php
 * @ingroup SemanticResultFormats
 *
 * @licence GNU GPL v2+
 * @author Vialibre
 */

if ( !defined( 'MEDIAWIKI' ) ) {
        die( 'Not an entry point.' );
}

class SRFAppFlow extends SMWResultPrinter {

	// configuration variables
	protected $m_isDebugSet	= false;
	protected $m_applicationCategory = 'Application';
	protected $m_applicationName = 'Application';

	// internal variables
        protected $m_appFlow; // AppFlow to be rendered

        /**
         * (non-PHPdoc)
         * @see SMWResultPrinter::handleParameters()
         */
        protected function handleParameters( array $params, $outputmode ) {
                parent::handleParameters( $params, $outputmode );

                // init application graph instance
                $this->m_appFlow = new AppFlowGraph();

		$this->m_applicationCategory = $params['applicationcat'];
		$this->m_applicationName = $params['applicationname'];
		$this->m_appFlow->setApplicationName( $params['applicationname'] );
		$this->m_isDebugSet = $params['debug'];
	}

        /**
         * @see SMWResultPrinter::getParamDefinitions
         *
         * @since 1.8
         *
         * @param $definitions array of IParamDefinition
         *
         * @return array of IParamDefinition|array
         */
        public function getParamDefinitions( array $definitions ) {
                $params = parent::getParamDefinitions( $definitions );

                $params['applicationcat'] = array(
                        'default' => 'Application',
                        'message' => 'srf-paramdesc-applicationcategory',
                );

                $params['applicationname'] = array(
                        'default' => 'Application',
                        'message' => 'srf-paramdesc-applicationname',
                );

		$params['debug'] = array(
			'type' => 'boolean',
			'default' => false,
			'message' => 'srf-paramdesc-debug',
		);

		return $params;
	}
	
	protected function getResultText( SMWQueryResult $res, $outputmode ) {
		if ( !is_callable( 'renderUML' ) ) {
			wfWarn( 'The SRF Process printer needs the PlantUML extension to be installed.' );
			return '[erreur] l\'extension plantUML n\'a pas pu être trouvée.';
		}

		global $wgContLang; // content language object

		$this->isHTML = true;

                //
                //      Iterate all rows in result set
                //

                $row = $res->getNext(); // get initial row (i.e. array of SMWResultArray)

                while ( $row !== false ) {
                        /* SMWDataItem */ $subject = $row[0]->getResultSubject(); // get Subject of the Result
                        // creates a new component if $val has type wikipage
                        if ( $subject->getDIType() == SMWDataItem::TYPE_WIKIPAGE ) {
                                $wikiPageValue = new SMWWikiPageValue( '_wpg' );
                                $wikiPageValue->setDataItem( $subject );
                                $flow = $this->m_appFlow->makeFlow( $wikiPageValue->getShortWikiText(), $wikiPageValue->getShortWikiText() );
                        }

	                //
                        //      Iterate all colums of the row (which describe properties of the application component)
                        //

                        foreach ( $row as $field ) {

                                // check column title
                                $req = $field->getPrintRequest();
                                switch ( ( strtolower( $req->getLabel() ) ) ) {

                                        case "hassourceapp":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $application = $this->m_appFlow->makeApplication( $val, $val );
                                                        $flow->addSource( $application );
                                                }
                                                break;

                                        case "hasdestinationapp":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $application = $this->m_appFlow->makeApplication( $val, $val );
                                                        $flow->addDestination( $application );
                                                }
                                                break;

                                        case "hassourcepartner":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $partner = $this->m_appFlow->makePartner( $val, $val );
                                                        $flow->addSource( $partner );
                                                }
                                                break;

                                        case "hasdestinationpartner":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $partner = $this->m_appFlow->makePartner( $val, $val );
                                                        $flow->addDestination( $partner );
                                                }
                                                break;

                                        case "hasflowtype":
                                                // should be only one
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $flow->setFlowType( $val );
                                                }

                                                break;

                               }
                        }

                        // reset row variables
                        unset( $flow );

                        $row = $res->getNext();         // switch to next row
                }

                //
                // generate graphInput
                //
                $graphInput = $this->m_appFlow->getPlantUMLCode();

                //
                // render plantUML code
                //
                $result = renderUML($graphInput, "", $GLOBALS['wgParser']);

                $debug = '';
                if ( $this->m_isDebugSet ) $debug = '<pre>' . $graphInput . '</pre>';

                return $result . $debug;

	}

}

/**
 * Class representing an application graph
 */
class AppFlowGraph {

	// configuration variables
	protected $m_showTypes		= true;		// should type icons be rendered ?

	// instance variables
	protected $m_applicationName	= ''; 	// the center node
	protected $m_flows		= array(); 	// list of all flows
	protected $m_applications	= array();	// list of applications
	protected $m_partners		= array();	// list of partners

        /**
         * This method should be used for getting new or existing components 
         * If a component does not exist yet, it will be created
         *
         * @param $id                   string, component id
         * @param $label                string, component label
         * @return                              Object of type ApplicationComponent
         */
        public function makeFlow( $id, $label ) {

                // check if flow exists

                if ( isset( $this->m_flows[$id] ) ) {
			// nothing to create

                } else {
                        // create new flow
                        $flow = new AppFlowFlow();
                        $flow->setId( $id );
                        $flow->setLabel( $label );
                        $flow->setAppFlow( $this );

                        // add new flow to appFlow
                        $this->m_flows[$id] = $flow;
                }

                return $this->m_flows[$id];

        }

        public function makeApplication( $id, $label ) {

                // check if server exists

                if ( isset( $this->m_applications[$id] ) ) {
			// nothing to create

                } else {
                        $application = new AppFlowApplication();
                        $application->setId( $id );
                        $application->setLabel( $label );

                        // add new server to application
                        $this->m_applications[$id] = $application;

                }

                return $this->m_applications[$id];

        }

        public function makePartner( $id, $label ) {

                // check if server exists

                if ( isset( $this->m_partners[$id] ) ) {
			// nothing to create

                } else {
                        $partner = new AppFlowPartner();
                        $partner->setId( $id );
                        $partner->setLabel( $label );

                        // add new server to partner
                        $this->m_partners[$id] = $partner;

                }

                return $this->m_partners[$id];

        }

        public function setShowTypes( $show ) {
                $this->m_showTypes = $show;
        }

        public function getShowTypes() {
                return $this->m_showTypes;
        }

	public function setApplicationName( $name ) {
		$this->m_applicationName = $name;
	}

	public function getApplicationName( $name ) {
		return $this->m_applicationName;
	}

	public function getPlantUMLCode() {
		//
		// header
		//

		$res =	"skinparam backgroundColor white\n".
			"skinparam hyperlinkColor black\n".
			"skinparam hyperlinkUnderline false\n".
			"skinparam interface {\n".
  			"BackgroundColor gold\n".
  			"BorderColor orange\n".
			"}\n".
			"skinparam database {\n".
  			"BackgroundColor white\n".
  			"BorderColor firebrick\n".
			"FontColor white\n".
			"}\n".
			"skinparam component {\n".
  			"Style uml2\n".
  			"BackgroundColor white\n".
  			"BorderColor #c80815\n".
			"FontColor white\n".
  			"ArrowColor black\n".
  			"ArrowFontColor black\n".
			"}\n";

		//
		// add applications
		//
		foreach ( $this->m_applications as $application ) {
			$res .= $application->getPlantUMLCode();
		}

		//
		// add partners
		//
		foreach ( $this->m_partners as $partner ) {
			$res .= $partner->getPlantUMLCode();
		}
		//
		// add flows
		//
		foreach ( $this->m_flows as $flow ) {
			$res .=	$flow->getPlantUMLCode();
		}	

                //
                // add final stuff
                //

                return $res;

        }

}

abstract class AppFlowElement {

        // TODO I18N
        private $m_id            = 'no_id';
        private $m_label         = 'unlabeled';
        private $m_uid;

        public function getUUID(){
                if (!isset($this->m_uid)){
                        $this->m_uid = sprintf( 'UUID%04x%04x%04x%04x%04x%04x%04x%04x',
                                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                                mt_rand(0, 0x0fff) | 0x4000,
                                mt_rand(0, 0x3fff) | 0x8000,
                                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
			
                }

                return $this->m_uid;
        }
        public function getId() {
                return $this->m_id;
        }

        public function setId( $id ) {
                $this->m_id = $id;
        }

        public function getLabel() {
                return $this->m_label;
        }

        public function setLabel( $label ) {
                $this->m_label = preg_replace('/.*:/','',$label);
        }

}

class AppFlowApplication extends AppFlowElement {

        private $m_flows	= array();

        public function getFlows() {
                return $this->m_flows;
        }

        public function addflow( $flow ) {
                $this->m_flows[] = $flow;
        }

        public function getPlantUMLCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

                // render application
		
		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

		$res .=	"component \"[[".
			$link." ".$this->getLabel().
			"]]\" as ".$this->getUUID()."\n";
		//TODO add icon <&browser> when the bug in PlantUML is fixed.

		return $res;
	}
		
}

class AppFlowPartner extends AppFlowElement {

        private $m_flows	= array();

        public function getFlows() {
                return $this->m_flows;
        }

        public function addflow( $flow ) {
                $this->m_flows[] = $flow;
        }

        public function getPlantUMLCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

                // render partner
		
		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

		$res =	"interface \"[[".
			$link." ".$this->getLabel().
			"]]\" as ".$this->getUUID()."\n";
		//TODO add icon <&external-link> when the bug in PlantUML is fixed.

		return $res;
	}
		
}

/**
 * Class representing an appFlow Flow 
 */
class AppFlowFlow extends AppFlowElement {

        private $m_appflow;				// reference to containing appFlow
        private $m_flowtype 	= '';			// type of flow

        private $m_source;      			// source app or partner for this flow
        private $m_destination;      			// destination app or partner for this flow

        public function setFlowType( $type ) {
                $this->m_flowtype = $type;
        }

        public function getFlowType() {
                return $this->m_flowtype;
        }

        public function setAppFlow( $appflow ) {
                $this->m_appflow =  $appflow;
        }

        public function getAppFlow() {
                return $this->m_appflow;
	}

        public function addSource( $component ) {
                $this->m_source = $component;
        }

        public function getSource() {
                return $this->m_source;
        }

        public function addDestination( $component ) {
                $this->m_destination = $component;
        }

        public function getDestination() {
                return $this->m_destination;
        }

        public function getPlantUMLCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		// render flow

		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);
	
		$typeIcon = "<&question-mark>";
		switch ($this->getFlowType()) {
		case "Manuel":
			$typeIcon = "<&person>";
			break;
		case "Automatique":
			$typeIcon = "<&cog>";
			break;
		}			

		$res =	$this->getSource()->getUUID().
			" -u-> ".
			$this->getDestination()->getUUID().
			" :".$typeIcon."[[".
			$link." ".$this->getLabel()."]]\n";

		return $res;
	}

}
