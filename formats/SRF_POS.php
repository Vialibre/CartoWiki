<?php

/**
 * SRF Printer to display a POS map
 * See https://fr.wikipedia.org/wiki/Urbanisation_%28informatique%29
 * 
 * @file SRF_POS.php
 * @ingroup SemanticResultFormats
 *
 * @licence GNU GPL v2+
 * @author Vialibre
 */

if ( !defined( 'MEDIAWIKI' ) ) {
        die( 'Not an entry point.' );
}

class SRFPOSMap extends SMWResultPrinter {

	// configuration variables
	protected $m_isDebugSet	= false;

	// internal variables

        /**
         * (non-PHPdoc)
         * @see SMWResultPrinter::handleParameters()
         */
        protected function handleParameters( array $params, $outputmode ) {
                parent::handleParameters( $params, $outputmode );

                // init POS graph instance
                $this->m_pos = new POSGraph();

		$this->m_isDebugSet = $params['debug'];
	}

        /**
         * @see SMWResultPrinter::getParamDefinitions
         *
         * @since 1.8
         *
         * @param $definitions array of IParamDefinition
         *
         * @return array of IParamDefinition|array
         */
        public function getParamDefinitions( array $definitions ) {
                $params = parent::getParamDefinitions( $definitions );

		$params['debug'] = array(
			'type' => 'boolean',
			'default' => false,
			'message' => 'srf-paramdesc-debug',
		);

		return $params;
	}

	protected function getResultText( SMWQueryResult $res, $outputmode ) {

		global $wgContLang; // content language object

		$this->isHTML = true;


                //
                //      Iterate all rows in result set
                //

                $row = $res->getNext(); // get initial row (i.e. array of SMWResultArray)

                while ( $row !== false ) {
                        $subject = $row[0]->getResultSubject(); // get Subject of the Result (SMWDataItem)
                        // creates a new component if $val has type wikipage
                        if ( $subject->getDIType() == SMWDataItem::TYPE_WIKIPAGE ) {
                                $wikiPageValue = new SMWWikiPageValue( '_wpg' );
                                $wikiPageValue->setDataItem( $subject );
                                $brick = $this->m_pos->makeBrick( $wikiPageValue->getPrefixedText(), $wikiPageValue->getText() );
                        }

	                //
                        //      Iterate all colums of the row (which describe properties of the application component)
                        //

                        foreach ( $row as $field ) {

                                // check column title
                                $req = $field->getPrintRequest();
                                switch ( ( strtolower( $req->getLabel() ) ) ) {

                           /*             case "hastype":

                                                // should be only one
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $ilot->setType( $val );
                                                }

                                                break;
				*/
                                        case "hasfunction":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
							$functionId = $wikiPageValue->getPrefixedText();

							$this->m_pos->addBrickToFunction( $brick, $functionId );

                                                }
                                                break;

                                        case "hasstate":
						// should be only one
                                                foreach ( $field->getContent() as $value ) {
                                                        $dataValue = new SMWStringValue( $field->getPrintRequest()->getTypeID() );
                                                        $dataValue->setDataItem( $value );
                                                        $val = $dataValue->getShortWikiText();

							$this->m_pos->addState($val);
                                                        $brick->setState($val);
                                                }
                                                break;
				

                               }
                        }

                        // reset row variables
                        unset( $brique );

                        $row = $res->getNext();         // switch to next row
                }

                //
                // generate POS
                //
                $result = $this->m_pos->getHtmlCode();

                $debug = '';
                if ( $this->m_isDebugSet ) $debug = '<pre><nowiki>' . $result . '</nowiki></pre>';

                return $result . $debug;

	}

}

/**
 * Class representing an application graph
 */
class POSGraph {

	// configuration variables
	protected $m_showTypes		= true;		// should type icons be rendered ?
	protected $m_showStates		= false;	// should states icons be rendered ?

	// instance variables
	protected $m_zones		= array(); 	// list of all zones
	protected $m_quartiers		= array();	// list of quartiers
	protected $m_ilots		= array();	// list of ilots
	protected $m_bricks		= array();	// list of content bricks
	protected $m_states		= array();	// list of states (used for legend)
	protected $m_types		= array();	// list of types (used for legend)

	// creates the initial Zones, Quartiers and Ilots.
	function __construct()
	{
		$this->initPOS ("Classement par domaine fonctionnel", 3);
	}

	function initPOS( $category, $depth = 1 ) {
		$wgMaxChildren = 500;
 
		$title = Title::newFromText( $category, NS_CATEGORY );
		
		if ( $title->getNamespace() != NS_CATEGORY ) {
			// Non-categories can't have children. :)
			return ;
		}
 
		$dbr = wfGetDB( DB_SLAVE );
 
		$inverse = false;
 
		$tables = array( 'page', 'categorylinks' );
		$fields = array( 'page_id', 'page_namespace', 'page_title',
			'page_is_redirect', 'page_len', 'page_latest', 'cl_to',
			'cl_from' );
		$where = array();
		$joins = array();
		$options = array( 'ORDER BY' => 'cl_type, cl_sortkey', 'LIMIT' => $wgMaxChildren );
 
		$joins['categorylinks'] = array( 'JOIN', 'cl_from = page_id' );
		$where['cl_to'] = $title->getDBkey();
		$options['USE INDEX']['categorylinks'] = 'cl_sortkey';
 
		$where['cl_type'] = 'subcat';
 
		# fetch member count if possible
		$tables = array_merge( $tables, array( 'category' ) );
		$fields = array_merge( $fields, array( 'cat_id', 'cat_title', 'cat_subcats', 'cat_pages', 'cat_files' ) );
		$joins['category'] = array( 'LEFT JOIN', array( 'cat_title = page_title', 'page_namespace' => NS_CATEGORY ) );
 
		$res = $dbr->select( $tables, $fields, $where, __METHOD__, $options, $joins );
 
		# collect categories separately from other pages
		$categories = '';
		$other = '';
 
		foreach ( $res as $row ) {
			$t = Title::newFromRow( $row );
			$cat = null;
 
			if ( $row->page_namespace == NS_CATEGORY ) {
				$cat = Category::newFromRow( $row, $t );
			} else {
				break;
			}
 
			// Instanciate the categories we found
			switch ($depth) {
				case 3 :
					$this->makeZone($t->getPrefixedText(), $t->getText());
					break;
				case 2 :
					$this->makeQuartier($t->getPrefixedText(), $t->getText(), $title->getPrefixedText());
					break;
				case 1 :
					$this->makeIlot($t->getPrefixedText(), $t->getText(), $title->getPrefixedText());
					break;
				case 0 :
					$this->makeFunction($t->getPrefixedText(), $t->getText(), $title->getPrefixedText());
					break;
			}

			// Go to the next deeper level
			if ($depth > 0) {
				$this->initPOS($t->getText(), $depth - 1);
			}
 
		}
 
		return ;
	}


        /**
         * This method should be used for getting new or existing components 
         * If a component does not exist yet, it will be created
         *
         * @param $id                   string, component id
         * @param $label                string, component label
         * @return                              Object of type ApplicationComponent
         */
        public function makeBrick( $id, $label ) {

                // check if function exists

                if ( isset( $this->m_bricks[$id] ) ) {
			// nothing to create

                } else {
                        // create new function
			wfWarn ("NEW FUNCTION : $id");
                        $brick = new POSBrick();
                        $brick->setId($id);
                        $brick->setLabel($label);

                        // the brick will be added later to its functions
                        $this->m_bricks[$id] = $brick;
                }

                return $this->m_bricks[$id];

        }

        public function makeFunction( $id, $label, $ilotId ) {

                // check if function exists

                if ( isset( $this->m_functions[$id] ) ) {
			// nothing to create

                } else {
                        // create new function
			wfWarn ("NEW FUNCTION : $id");
                        $function = new POSFunction();
                        $function->setId($id);
                        $function->setLabel($label);

                        // add new function to POS map 
                        $this->m_functions[$id] = $function;
			$function->setParentElement($this->m_ilots[$ilotId]);
			$this->m_ilots[$ilotId]->addFunction($function);
                }

                return $this->m_functions[$id];

        }

        public function makeIlot( $id, $label, $quartierId ) {

                // check if ilot exists

                if ( isset( $this->m_ilots[$id] ) ) {
			// nothing to create

                } else {
                        // create new ilot
			wfWarn ("NEW ILOT : $id");
                        $ilot = new POSIlot();
                        $ilot->setId($id);
                        $ilot->setLabel($label);

                        // add new ilot to POS map
                        $this->m_ilots[$id] = $ilot;
			$ilot->setParentElement($this->m_quartiers[$quartierId]);
			$this->m_quartiers[$quartierId]->addIlot($ilot);
			$ilot->setGraph($this);
                }

                return $this->m_ilots[$id];

        }

        public function makeQuartier( $id, $label, $zoneId ) {

                // check if quartier exists

                if ( isset( $this->m_quartiers[$id] ) ) {
			// nothing to create

                } else {
			// create new quartier
                        $quartier = new POSQuartier();
                        $quartier->setId($id);
                        $quartier->setLabel($label);

                        // add new quartier to POS map
                        $this->m_quartiers[$id] = $quartier;
			$quartier->setParentElement($this->m_zones[$zoneId]);
			$this->m_zones[$zoneId]->addQuartier($quartier);

                }

                return $this->m_quartiers[$id];

        }

        public function makeZone( $id, $label ) {

                // check if zone exists

                if ( isset( $this->m_zones[$id] ) ) {
			// nothing to create

                } else {
			// create new zone
                        $zone = new POSZone();
                        $zone->setId( $id );
                        $zone->setLabel( $label );

                        // add new zone to POS map
                        $this->m_zones[$id] = $zone;

                }

                return $this->m_zones[$id];

        }

	public function addBrickToFunction ($brick, $functionId) {
		if ( isset( $this->m_functions[$functionId] ) ) {
			// the function exists
			$function = $this->m_functions[$functionId];
			// enable the function and its parent elements
			$element = $function;
			while ($element != null) {
				$element->addBrick($brick);
				$element->enable();
				$element = $element->getParentElement();
			}

		} else {
			// do nothing
		}
	}

	public function addState ($state) {

		if ( isset( $this->m_states[$state] ) ) {
			// nothing to do
		} else {
			$this->m_states[$state] = $state;
		}

		wfWarn ("addState : $state");


		// activate labels and legend in case of states
		if ( count($this->m_states) > 1 ) {
			$this->setShowStates(true);
		}
	}
	
        public function setShowTypes( $show ) {
                $this->m_showTypes = $show;
        }

        public function getShowTypes() {
                return $this->m_showTypes;
        }

        public function setShowStates( $show ) {
                $this->m_showStates = $show;
        }

        public function getShowStates() {
                return $this->m_showStates;
        }

	public function getHtmlCode() {
		//
		// header
		//
		$res =	"<div class=\"Graph_couverture_fonctionnelle\">\n";

		//
		// for each zone
		//
		
		foreach ($this->m_zones as $zone) {
			$res .= $zone->getHtmlCode();	
		}

                //
                // add final stuff
                //
		$res .=	"</div>\n";

		//
		// add legend
		//
		$legend =	 "<table class=\"pos-legend\" style=\"margin: 2px; width:100%;\">\n"
				."  <tbody>\n"
		       		."    <tr><th>Affichage :</th></tr>\n"
		 		."    <tr><td><input type=\"checkbox\" name=\"cw_show\" checked=\"checked\"/> Vue synthétique\n"
				."    </td></tr>\n"
				."    <tr><td>Détails : <input type=\"radio\" name=\"cw_scale\" value=\"4\" checked=\"checked\"> ZQIF - \n"
				."		        <input type=\"radio\" name=\"cw_scale\" value=\"3\"> ZQI - \n"
				."		        <input type=\"radio\" name=\"cw_scale\" value=\"2\"> ZQ - \n"
				."		        <input type=\"radio\" name=\"cw_scale\" value=\"1\"> Z\n"
				."    </td></tr>\n"
				."  </tbody>\n"
				."</table>\n";

                return $legend.$res;

        }

}

abstract class POSElement {

        // TODO I18N
        private $m_id            = 'no_id';
        private $m_label         = 'unlabeled';
        private $m_uid;

	private $m_enabled	 = false;
	private $m_parentElement = null;

        private $m_bricks    = array();

        public function getUUID(){
                if (!isset($this->m_uid)){
                        $this->m_uid = sprintf( 'UUID%04x%04x%04x%04x%04x%04x%04x%04x',
                                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                                mt_rand(0, 0x0fff) | 0x4000,
                                mt_rand(0, 0x3fff) | 0x8000,
                                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
			
                }

                return $this->m_uid;
        }
        public function getId() {
                return $this->m_id;
        }

        public function setId( $id ) {
                $this->m_id = $id;
        }

        public function getLabel() {
                return $this->m_label;
        }

        public function setLabel( $label ) {
                $this->m_label = $label;
        }

        public function getParentElement() {
                return $this->m_parentElement;
        }

        public function setParentElement( $parent ) {
                $this->m_parentElement = $parent;
        }

        public function getBricks() {
                return $this->m_bricks;
        }

        public function addBrick( $brick ) {
                $this->m_bricks[$brick->getId()] = $brick;
        }

	public function enable() {
		$this->m_enabled = true;
	}

	public function isEnabled() {
		return $this->m_enabled === true;
	}

}

class POSZone extends POSElement {

        private $m_quartiers	= array();

        public function getQuartiers() {
                return $this->m_quartiers;
        }

        public function addQuartier( $quartier ) {
                $this->m_quartiers[] = $quartier;
        }

        public function getHtmlCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

                // render zone
		$res = '';
		if ($this->isEnabled()) {
                	$res .=	"<div class=\"zone used\">\n";
		} else {
			$res .= "<div class=\"zone empty\">\n";
		}
		$res .= "  <div class=\"titre\">";
		$res .=	"    <a href=\"$link\">".$this->getLabel()."</a>";
		$res .=	"  </div>\n";
		// render bricks
		foreach ($this->getBricks() as $brick) {
			$res .= $brick->getHtmlCode();
		}
		// render quartiers
		foreach ($this->getQuartiers() as $quartier) {
			$res .= $quartier->getHtmlCode();
		}
		// end of zone
		$res .= "</div>\n";

		return $res;
	}
		
}

class POSQuartier extends POSElement {

        private $m_ilots	= array();

        public function getIlots() {
                return $this->m_ilots;
        }

        public function addIlot( $ilot ) {
                $this->m_ilots[] = $ilot;
        }

        public function getHtmlCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

                // render quartier
		$res = '';
		if ($this->isEnabled()) {
                	$res .=	"<div class=\"quartier used\">\n";
		} else {
			$res .= "<div class=\"quartier empty\">\n";
		}
		$res .= "  <div class=\"titre\">";
		$res .=	"    <a href=\"$link\">".$this->getLabel()."</a>";
		$res .=	"  </div>\n";
		// render bricks
		foreach ($this->getBricks() as $brick) {
			$res .= $brick->getHtmlCode();
		}
		// render ilots
		foreach ($this->getIlots() as $ilot) {
			$res .= $ilot->getHtmlCode();
		}
		// end of quartier
		$res .= "</div>\n";

		return $res;
	}
		
}

class POSIlot extends POSElement {

	private $m_type = "";
	private $m_graph;

        private $m_functions	= array();

        public function getFunctions() {
                return $this->m_functions;
        }

        public function addFunction( $function ) {
                $this->m_functions[] = $function;
        }

        public function getType() {
                return $this->m_type;
        }

        public function setType( $type ) {
                $this->m_type = $type;
	}

        public function setGraph( $graph ) {
                $this->m_graph = $graph;
	}

        public function getHtmlCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

                // render ilot
		$res = '';
		if ($this->isEnabled()) {
                	$res .=	"<div class=\"func1 used\">\n";
		} else {
			$res .= "<div class=\"func1 empty\">\n";
		}
		$res .= "  <div class=\"titre\">";
		$res .=	"    <a href=\"$link\">".$this->getLabel()."</a>";
		$res .=	"  </div>\n";
		// render bricks
		foreach ($this->getBricks() as $brick) {
			$res .= $brick->getHtmlCode();
		}
		// render functions ??
		foreach ($this->getFunctions() as $function) {
			$res .= $function->getHtmlCode();
		}
		// end of quartier
		$res .= "</div>\n";

		return $res;
	}
		
}

class POSFunction extends POSElement {

        public function getHtmlCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

                // render function
		if ($this->isEnabled()) {
                	$res .=	"<div class=\"func2 used\">\n";
		} else {
			$res .= "<div class=\"func2 empty\">\n";
		}
		$res .= "  <div class=\"titre\">";
		$res .= "    <div class=\"contenu\">";
		$res .=	"      <a href=\"$link\">".$this->getLabel()."</a>";
		$res .=	"    </div>\n";
		$res .=	"  </div>\n";
		// render bricks
		foreach ($this->getBricks() as $brick) {
			$res .= $brick->getHtmlCode();
		}
		//
		// end of function
		$res .= "</div>\n";

		return $res;
	}
		
}

class POSBrick extends POSElement {

	private $m_state = "";

        public function getState() {
                return $this->m_state;
        }

        public function setState( $state ) {
                $this->m_state = $state;
	}

        public function getHtmlCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',$link,$wgArticlePath);

		$state = preg_replace('/ /','_',$this->getState());

                // render function
		$res = 	"<div class=\"element\">";
                $res .=	"  <div class=\"contenu application $state\">\n";
		$res .=	"    <a href=\"$link\">".$this->getLabel()."</a>";
		$res .= "  </div>\n";
		$res .= "</div>\n";

		return $res;
	}
		
}
