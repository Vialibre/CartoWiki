<?php
/**
 * SRF Printer to display a process graph using plantuml.
 * 
 * @file SRF_CartoApplication.php
 * @ingroup SemanticResultFormats
 *
 * @licence GNU GPL v2+
 * @author Vialibre
 */

if ( !defined( 'MEDIAWIKI' ) ) {
	die( 'Not an entry point.' );
}

class SRFUMLProcess extends SMWResultPrinter {

	// configuration variables
	protected $m_isDebugSet		= false;

	// internal variables
	protected $m_process;	// process to be rendered

	/**
	 * (non-PHPdoc)
	 * @see SMWResultPrinter::handleParameters()
	 */
	protected function handleParameters( array $params, $outputmode ) {
		parent::handleParameters( $params, $outputmode );

		// init process graph instance
		$this->m_process = new UMLProcessGraph();

		$this->m_process->setGraphName( trim( $params['graphname'] ) );

		$this->m_process->setShowActors( $params['showactors'] );
		$this->m_process->setShowFunctionalities( $params['showfunctionalities'] );

		$this->m_isDebugSet = $params['debug'];
	}

	/**
	 * @see SMWResultPrinter::getParamDefinitions
	 *
	 * @since 1.8
	 *
	 * @param $definitions array of IParamDefinition
	 *
	 * @return array of IParamDefinition|array
	 */
	public function getParamDefinitions( array $definitions ) {
		$params = parent::getParamDefinitions( $definitions );

		$params['graphname'] = array(
			'default' => '',
			'message' => 'srf-paramdesc-graphname',
		);

		$params['showactors'] = array(
			'type' => 'boolean',
			'default' => false,
			'message' => 'srf-paramdesc-showactors',
		);

		$params['showfunctionalities'] = array(
			'type' => 'boolean',
			'default' => false,
			'message' => 'srf-paramdesc-showfunctionalities',
		);

		$params['debug'] = array(
			'type' => 'boolean',
			'default' => false,
			'message' => 'srf-paramdesc-debug',
		);

		return $params;
	}

	/**
	 *	This method renders the result set provided by SMW according to the printer
	 *
	 *  @param res				SMWQueryResult, result set of the ask query provided by SMW
	 *  @param outputmode		?
	 *  @return				String, rendered HTML output of this printer for the ask-query
	 *
	 */
	protected function getResultText( SMWQueryResult $res, $outputmode ) {
		switch ($this->m_process->getRenderer()) {
			case "uml":
				if ( !is_callable( 'renderUML' ) ) {
                	        	wfWarn( 'The SRF Process printer needs the PlantUML extension to be installed.' );
                        		return '';
                		}
				break;
			}

		global $wgContLang; // content language object

		//
		//	render as HTML
		//
		$this->isHTML 		= true;


		//
		//	Iterate all rows in result set
		//

		$row = $res->getNext(); // get initial row (i.e. array of SMWResultArray)

		while ( $row !== false ) {
			/* SMWDataItem */ $subject = $row[0]->getResultSubject(); // get Subject of the Result
			// creates a new node if $val has type wikipage
			if ( $subject->getDIType() == SMWDataItem::TYPE_WIKIPAGE ) {
				$wikiPageValue = new SMWWikiPageValue( '_wpg' );
				$wikiPageValue->setDataItem( $subject );
				$node = $this->m_process->makeNode( $wikiPageValue->getShortWikiText(), $wikiPageValue->getShortWikiText() );
			}

			//
			//	Iterate all colums of the row (which describe properties of the process node)
			//

			/**
			 * @var SMWResultArray $field
			 */
			foreach ( $row as $field ) {

				// check column title
				$req = $field->getPrintRequest();
				switch ( ( strtolower( $req->getLabel() ) ) ) {

	 				case "haslabel":
	 					$value = current($field->getContent()); // save only the first

						if (($value !== false)) {
							$wikiPageValue = new SMWWikiPageValue( '_wpg' );
							$wikiPageValue->setDataItem( $value );
							$val = $wikiPageValue->getLongWikiText();

							$val = str_replace("&","and",$val);
							$node->setLabel($val);
						
						}
						break;

					case "hasactor":
						foreach ( $field->getContent() as $value ) {
							$wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
							$wikiPageValue->setDataItem( $value );
							$val = $wikiPageValue->getShortWikiText();

							$actor = $this->m_process->makeActor( $val, $val );
							$node->addActor( $actor );
						}
						break;

					case "usesfunctionality":
						foreach ( $field->getContent() as $value ) {
							$wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
							$wikiPageValue->setDataItem( $value );
							$val = $wikiPageValue->getShortWikiText();

							$func = $this->m_process->makeFunctionality( $val, $val );
							$node->addUsedFunctionality( $func );
						}
						break;

					case "hassuccessor":

						if ( count( $field->getContent() ) > 1 ) {

							// SplitParallel
							$edge = new UMLSplitParallelEdge();
							$edge->setFrom( $node );
							foreach ( $field->getContent() as $value ) {
								$wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
								$wikiPageValue->setDataItem( $value );
								$val = $wikiPageValue->getShortWikiText();

								$edge->addTo( $this->m_process->makeNode( $val, $val ) );
							}

						} else {

							// Sequence
							foreach ( $field->getContent() as $value ) {
								$wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
								$wikiPageValue->setDataItem( $value );
								$val = $wikiPageValue->getShortWikiText();

								$edge = new UMLSequentialEdge();
								$edge->setFrom( $node );
								$edge->setTo( $this->m_process->makeNode( $val, $val ) );
							}
						}

						break;

					default:

						// Ignore column

	 			}
			}

			// reset row variables
			unset( $node );

		  	$row = $res->getNext();		// switch to next row
		}

		//
		// generate graphInput
		//
		$result = "";
		switch ($this->m_process->getRenderer()) {
			case "uml":
				$graphInput = $this->m_process->getPlantUMLCode();
				$result = renderUML($graphInput, "", $GLOBALS['wgParser']);
				break;
		}

		$debug = '';
		if ( $this->m_isDebugSet ) $debug = '<pre>' . $graphInput . '</pre>';

		return $result . $debug;
	}
}

/**
 * Class representing a process graph
 */
class UMLProcessGraph {

	// configuration variables
	protected $m_graphName 		= '';
	protected $m_renderer		= 'uml';
	protected $m_showActors		= false;	// should actors be rendered?
	protected $m_showFunctionalities	= false;	// should functionalities be rendered?

	public $m_useHtmlNodes = true;			// Set to false if you do not want to use HTML table nodes

	// instance variables
	protected $m_nodes		= array();	// list of all nodes
	protected $m_startnodes	= array();	// list of start nodes
	protected $m_endnodes	= array();	// list of end nodes
	protected $m_functionalities	= array();	// list of functionalities
	protected $m_actors		= array();	// list of actors
	protected $m_errors		= array();	// list of errors


	/**
	 * This method should be used for getting new or existing nodes
	 * If a node does not exist yet, it will be created
	 *
	 * @param $id			string, node id
	 * @param $label		string, node label
 	 * @return				Object of type ProcessNode
	 */
	public function makeNode( $id, $label ) {
		// check if node exists
		if ( isset( $this->m_nodes[$id] ) ) {
			// take existing node
			$node = $this->m_nodes[$id];

		} else {
			// create new node

			$node = new UMLProcessNode();
			$node->setId( $id );
			$node->setLabel( $label );
			$node->setProcess( $this );

			// add new node to process
			$this->m_nodes[$id] = $node;
		}

		return $node;

	}

	public function makeActor( $id, $label ) {
		// check if actor exists
		if ( isset( $this->m_actors[$id] ) ) {
			// take existing actors
			$actor = $this->m_actors[$id];

		} else {
			$actor = new UMLProcessActor();
			$actor->setId( $id );
			$actor->setLabel( $label );

			// add new actor to process
			$this->m_actors[$id] = $actor;
		}

		return $actor;

	}

	public function makeFunctionality( $id, $label ) {
		// check if res exists
		if ( isset( $this->m_functionalities[$id] ) ) {
			// take existing func 
			$func = $this->m_functionalities[$id];

		} else {
			$func = new UMLProcessFunctionality();
			$func->setId( $id );
			$func->setLabel( $label );

			// add new res to process
			$this->m_functionalities[$id] = $func;

		}

		return $func;

	}

	public function getEndNodes() {
		if ( count( $this->m_endnodes ) == 0 ) {
			foreach ( $this->m_nodes as $node ) {
				if ( count( $node->getSucc() ) == 0 ) $this->m_endnodes[] = $node;
			}
		}

		return $this->m_endnodes;
	}

	public function getStartNodes() {

		if ( count( $this->m_startnodes ) == 0 ) {
			foreach ( $this->m_nodes as $node ) {
				if ( count( $node->getPred() ) == 0 ) {
					$this->m_startnodes[] = $node;
				}
			}
		}

		return $this->m_startnodes;
	}

	public function setShowActors( $show ) {
		$this->m_showActors = $show;
	}

	public function getShowActors() {
		return $this->m_showActors;
	}

	public function setShowFunctionalities( $show ) {
		$this->m_showFunctionalities = $show;
	}

	public function getShowFunctionalities() {
		return $this->m_showFunctionalities;
	}

	public function setGraphName( $name ) {
		$this->m_graphName = $name;
	}

	public function setRenderer( $name ) {
		$this->m_renderer = $name;
	}

	public function getRenderer() {
		return $this->m_renderer;
	}

	public function getGraphName() {
		if ( $this->m_graphName == '' ) $this->m_graphName = 'ProcessQueryResult' . rand( 1, 99999 );
		return $this->m_graphName;
	}

	public function addError( $error ) {
		$this->m_errors[] = $error;
	}

	public function getErrors() {
		return $this->m_errors;
	}

        public function getPlantUMLCode() {
                //
                // header
                //
                $res =  "skinparam backgroundColor white\n".
                        "skinparam hyperlinkColor black\n".
                        "skinparam hyperlinkUnderline false\n".
			"skinparam shadowing true\n".
                        "\n".
			"skinparam NoteBackgroundColor #d8bfd8\n".
			"skinparam NoteBorderColor darkmagenta\n".
                        "skinparam activity {\n".
                        "ArrowColor    black\n".
                        "BackgroundColor       #e0eeee\n".
                        "BorderColor   #2b6398\n".
                        "StartColor    #228b22\n".
                        "EndColor      #8b2323\n".
                        "BarColor      grey\n".
                        "}\n";

                //
                // for each startnodes
                //

                foreach ( $this->getStartNodes() as $startnode ) {
                        // display a start point
                        $res .= $startnode->getPlantUMLCode(true);
                        // then follow till the end
                        $node = $startnode;
                        while ( in_array($node, $this->getEndNodes()) === false ) {
                                $nb_succ = count($node->getSucc());
                                foreach ($node->getSucc() as $k => $v){
                                        if ($nb_succ <= 1){
                                                $res .= $v->getPlantUMLCode();
                                        } else {
                                                if ($k === 0){
                                                        $res.=  "fork\n".
                                                                $v->getPlantUMLCode();
                                                } elseif ($nb_succ === $k+1) {
                                                        $res.=  "fork again\n".
                                                                $v->getPlantUMLCode().
                                                                "end fork\n";
                                                } else {
                                                        $res.=  "fork again\n".
                                                                $v->getPlantUMLCode();
                                                }
                                        }
                                        //$res .= "key=".$k."; val=".$v->getPlantUMLCode().";\n";
                                }
                                $node = $node->getSucc()[0];
                                //$res .= $node->getPlantUMLCode;
                        }
                        $res .= "stop\n";

                }

                //
                // add final stuff
                //

                return $res;
	}

}

abstract class UMLProcessElement {

	// TODO I18N
	private $m_id		 = 'no_id';
	private $m_label	 = 'unlabeled';
	private $m_uid;

	public function getUUID(){
		if (!isset($this->m_uid)){
			$this->m_uid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
				mt_rand(0, 0x0fff) | 0x4000,
				mt_rand(0, 0x3fff) | 0x8000,
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
		}

		return $this->m_uid;
	}
	public function getId() {
		return $this->m_id;
	}

	public function setId( $id ) {
		$this->m_id = $id;
	}

	public function getLabel() {
		return $this->m_label;
	}

	public function getWrappedLabel() {
		return wordwrap($this->m_label, 15, "\n");
	}

	public function setLabel( $label ) {
		//$this->m_label = $label;
		$this->m_label = preg_replace('/.*:/','',$label);
	}

}

class UMLProcessFunctionality extends UMLProcessElement {

	private $m_usedby		= array();

	public function getUsers() {
		return $this->m_usedby;
	}

	public function addUser( $node ) {
		$this->m_usedby[] = $node;
	}

}

class UMLProcessActor extends UMLProcessElement {

	private $m_nodes	= array();

	public function getNodes() {
		return $this->m_nodes;
	}

	public function addNode( $node ) {
		$this->m_nodes[] = $node;
	}

}

/**
 * Class reperesning a process node
 */
class UMLProcessNode extends UMLProcessElement {

	private $m_is_startnode	= false;	// explicit statement if this is a start node
	private $m_is_endnode	= false;	// explicit statement if this is a termination node

	private $m_process;					// reference to parent process

	private $m_usedfunctionalities	= array();	// functionalities used by this node
	private $m_actors		= array();	// actors related to this node

	private $m_edgeout;					// outgoing edge (can be only one)
	private	$m_edgesin 	=	array();	// incoming edges (can be many)

	public function setProcess( $proc ) {
		$this->m_process =  $proc;
	}

	public function getProcess() {
		return $this->m_process;
	}

	public function getPred() {
		$res = array();

		foreach ( $this->m_edgesin as $edge ) {
			$res = array_merge( $res, $edge->getPred() );
		}

		return $res;
	}

	public function getSucc() {
		$res = array();

		if ( isset( $this->m_edgeout ) ) {
			$res = $this->m_edgeout->getSucc();
		}

		return $res;
	}

	public function setEdgeOut( $edge ) {
		$this->m_edgeout = $edge;
	}

	public function getEdgeOut() {
		return $this->m_edgeout;
	}

	public function addEdgeIn( $edge ) {
		$this->m_edgesin[] = $edge;
	}

	public function getEdgesIn() {
		return $this->m_edgesin;
	}

	public function addActor( $actor ) {
		$this->m_actors[] = $actor;
		$actor->addNode( $this );
	}

	public function getActors() {
		return $this->m_actors;
	}

	public function addUsedFunctionality( $func ) {
		$this->m_usedfunctionalities[] = $func;
		$func->addUser( $this );
	}

	public function getUsedFunctionalities() {
		return $this->m_usedfunctionalities;
	}

	public function getPlantUMLCode($is_startnode = false) {
                global $IP, $wgArticlePath;
                // render node
                $res = "";
                if ($this->m_process->m_useHtmlNodes){
                        $link = preg_replace('/ /','_',$this->getId());
                        $link = str_replace('$1',$link,$wgArticlePath);
                        $res =  ":[[".
                                $link.
                                " ".
                                $this->getLabel().
                                "]]";
                } else {
                        $res = ":".$this->getLabel();
                }
                // render functionalities
                if ( $this->getProcess()->getShowFunctionalities() ) {
                        if ( count($this->getUsedFunctionalities()) > 0 ){
                                foreach ( $this->getUsedFunctionalities() as $functionality ) {
                                        $link = preg_replace('/ /','_',$functionality->getId());
                                        $link = str_replace('$1',$link,$wgArticlePath);
                                        $res =  $res.
                                                "\n....\n<size:8><back:#d8bfd8>[[".
                                                $link.
                                                " ".
                                                $functionality->getLabel().
                                                "]]</back></size>";
				}
                        }
                }
		$res .= ";\n";

                // add start point before the swinlane
                if ($is_startnode) {
                       $res = "start\n".$res;
                }
                // render swimlane for the actor
                if ( $this->getProcess()->getShowActors() ) {
                        if (count($this->getActors())>0){
                                $actors_uml = "|";
                                foreach ( $this->getActors() as $actor ) {
                                        $link = preg_replace('/ /','_',$actor->getId());
                                        $link = str_replace('$1',$link,$wgArticlePath);
                                        $actors_uml .=  "[[".
                                                $link.
                                                " ".
                                                $actor->getLabel().
                                                "]] ";
                                }
                                $actors_uml .= "|\n";
                                $res = $actors_uml.$res;
                        }

                }

                return $res;
        }


}


/**
 * Abstract base class for edges in a process graph
 */
abstract class UMLProcessEdge{

	private $m_id;
	private $m_uid;

	public function getId(){
		if (!isset($this->m_id)){
			$this->m_id = 'edge' . rand(1, 99999);
		}

		return $this->m_id;
	}

	public function getUUID(){
		if (!isset($this->m_uid)){
			$this->m_uid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    	mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    	mt_rand(0, 0x0fff) | 0x4000,
    	mt_rand(0, 0x3fff) | 0x8000,
    	mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
		}

		return $this->m_uid;
	}

	abstract public function getSucc();
	abstract public function getPred();

}

abstract class UMLSplitEdge extends UMLProcessEdge{

	protected $m_from;
	protected $m_to 	= array();

	public function setFrom($node){
		$this->m_from = $node;
		$node->setEdgeOut($this);
	}

	public function addTo($node){
		$this->m_to[] = $node;
		$node->addEdgeIn($this);
	}

	public function getPred(){
		return array($this->m_from);
	}

	public function getSucc(){
		return $this->m_to;


	}

}

class UMLSplitParallelEdge extends UMLSplitEdge{

}

class UMLSequentialEdge extends UMLProcessEdge{

        private $m_from;
        private $m_to;

        public function setFrom($node){
                $this->m_from = $node;
                $node->setEdgeOut($this);
        }

        public function setTo($node){
                $this->m_to = $node;
                $node->addEdgeIn($this);
        }

        public function getPred(){
                return array($this->m_from);
        }

        public function getSucc(){
                return array($this->m_to);
        }

}
