<?php

/**
 * SRF Printer to display a map of an application and its components
 * 
 * @file SRF_CartoApplication.php
 * @ingroup SemanticResultFormats
 *
 * @licence GNU GPL v2+
 * @author Vialibre
 */

if ( !defined( 'MEDIAWIKI' ) ) {
        die( 'Not an entry point.' );
}

class SRFApplication extends SMWResultPrinter {

	// configuration variables
	protected $m_isDebugSet	= false;
	protected $m_applicationCategory = 'Application';
	protected $m_mainApp = 'Application';

	// internal variables
        protected $m_application; // application to be rendered

        /**
         * (non-PHPdoc)
         * @see SMWResultPrinter::handleParameters()
         */
        protected function handleParameters( array $params, $outputmode ) {
                parent::handleParameters( $params, $outputmode );

                // init application graph instance
                $this->m_application = new ApplicationGraph();

		$this->m_applicationCategory = $params['applicationcat'];
		$this->m_mainApp = $params['applicationname'];
		$this->m_application->setMainApp( $params['applicationname'] );
		$this->m_isDebugSet = $params['debug'];
	}

        /**
         * @see SMWResultPrinter::getParamDefinitions
         *
         * @since 1.8
         *
         * @param $definitions array of IParamDefinition
         *
         * @return array of IParamDefinition|array
         */
        public function getParamDefinitions( array $definitions ) {
                $params = parent::getParamDefinitions( $definitions );

                $params['applicationcat'] = array(
                        'default' => 'Application',
                        'message' => 'srf-paramdesc-applicationcategory',
                );

                $params['applicationname'] = array(
                        'default' => 'Application',
                        'message' => 'srf-paramdesc-applicationname',
                );

		$params['debug'] = array(
			'type' => 'boolean',
			'default' => false,
			'message' => 'srf-paramdesc-debug',
		);

		return $params;
	}
	
	protected function getResultText( SMWQueryResult $res, $outputmode ) {
		if ( !is_callable( 'renderUML' ) ) {
			wfWarn( 'The SRF Process printer needs the PlantUML extension to be installed.' );
			return '[erreur] l\'extension plantUML n\'a pas pu être trouvée.';
		}

		global $wgContLang; // content language object

		$this->isHTML = true;

                //
                //      Iterate all rows in result set
                //

                $row = $res->getNext(); // get initial row (i.e. array of SMWResultArray)

                while ( $row !== false ) {
                        /* SMWDataItem */ $subject = $row[0]->getResultSubject(); // get Subject of the Result
                        // creates a new component if $val has type wikipage
                        if ( $subject->getDIType() == SMWDataItem::TYPE_WIKIPAGE ) {
                                $wikiPageValue = new SMWWikiPageValue( '_wpg' );
                                $wikiPageValue->setDataItem( $subject );
                                $component = $this->m_application->makeComponent( $wikiPageValue->getShortWikiText(), $wikiPageValue->getShortWikiText() );
                        }

	                //
                        //      Iterate all colums of the row (which describe properties of the application component)
                        //

                        foreach ( $row as $field ) {

                                // check column title
                                $req = $field->getPrintRequest();
                                switch ( ( strtolower( $req->getLabel() ) ) ) {

                                        case "hastype":

                                                // should be only one
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $component->setComponentType( $val );
                                                }

                                                break;

                                        case "hasserver":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $server = $this->m_application->makeServer( $val, $val );
                                                        $component->addServer( $server );
                                                }
                                                break;

                                        case "hasapplication":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $app = $this->m_application->makeApp( $val, $val );
                                                        $component->addApp( $app );
                                                }
                                                break;

					case "hasconnection":
                                                foreach ( $field->getContent() as $value ) {
                                                        $wikiPageValue = new SMWWikiPageValue( $field->getPrintRequest()->getTypeID() );
                                                        $wikiPageValue->setDataItem( $value );
                                                        $val = $wikiPageValue->getShortWikiText();

                                                        $connectedComponent = $this->m_application->makeComponent( $val, $val );
                                                        $component->addConnection( $connectedComponent );
                                                }
                                                break;

					case "hasconnectiontype":
                                                foreach ( $field->getContent() as $value ) {
                                                        $dataValue = new SMWStringValue( $field->getPrintRequest()->getTypeID() );
                                                        $dataValue->setDataItem( $value );
                                                        $val = $dataValue->getShortWikiText();

                                                        $component->setConnectionType( $val );
                                                }
                                                break;

                               }
                        }

                        // reset row variables
                        unset( $component );

                        $row = $res->getNext();         // switch to next row
                }

                //
                // generate graphInput
                //
                $graphInput = $this->m_application->getPlantUMLCode();

                //
                // render plantUML code
                //
                $result = renderUML($graphInput, "", $GLOBALS['wgParser']);

                $debug = '';
                if ( $this->m_isDebugSet ) $debug = '<pre>' . $graphInput . '</pre>';

                return $result . $debug;

	}

}

/**
 * Class representing an application graph
 */
class ApplicationGraph {

	// configuration variables
	protected $m_showTypes		= true;		// should type icons be rendered ?

	// instance variables
	protected $m_mainapp		= ''; 		// name of main application
	protected $m_components		= array(); 	// list of all components
	protected $m_servers		= array();	// list of servers
	protected $m_apps		= array();	// list of apps

        /**
         * This method should be used for getting new or existing components 
         * If a component does not exist yet, it will be created
         *
         * @param $id                   string, component id
         * @param $label                string, component label
         * @return                              Object of type ApplicationComponent
         */
        public function makeComponent( $id, $label ) {

                // check if component exists

                if ( isset( $this->m_components[$id] ) ) {
			// nothing to create

                } else {
                        // create new component
			wfWarn ("NEW COMPONENT : $id");
                        $component = new ApplicationComponent();
                        $component->setId( $id );
                        $component->setLabel( $label );
                        $component->setApplication( $this );

                        // add new component to application
                        $this->m_components[$id] = $component;
                }

                return $this->m_components[$id];

        }

        public function makeServer( $id, $label ) {

                // check if server exists

                if ( isset( $this->m_servers[$id] ) ) {
			// nothing to create

                } else {
                        $server = new ApplicationServer();
                        $server->setId( $id );
                        $server->setLabel( $label );

                        // add new server to application
                        $this->m_servers[$id] = $server;

                }

                return $this->m_servers[$id];

        }

        public function makeApp( $id, $label ) {

                // check if app exists

                if ( isset( $this->m_apps[$id] ) ) {
			// nothing to create

                } else {
                        $server = new ApplicationApp();
                        $server->setId( $id );
                        $server->setLabel( $label );

                        // add new app to application
                        $this->m_apps[$id] = $server;

                }

                return $this->m_apps[$id];

        }

        public function setShowTypes( $show ) {
                $this->m_showTypes = $show;
        }

        public function getShowTypes() {
                return $this->m_showTypes;
        }

	public function setMainApp( $name ) {
		$this->m_mainapp = $name;
	}

	public function getMainApp( $name ) {
		return $this->m_mainapp;
	}

	public function getPlantUMLCode() {
		//
		// header
		//
		$res =	"skinparam backgroundColor white\n".
			"skinparam hyperlinkColor black\n".
			"skinparam hyperlinkUnderline false\n".
			"skinparam package {\n".
			"  backgroundColor #f2f2f2\n".
			"}\n".
			"skinparam frame {\n".
  			"  backgroundColor #9ed08a\n".
  			"  BorderColor #556b2f\n".
			"}\n".
			"skinparam component {\n".
  			"  BackgroundColor #white\n".
  			"  BorderColor #8b0000\n".
  			"  ArrowColor black\n".
  			"  ArrowFontColor dimgrey\n".
			"}\n";

		//
		// add main application
		//
		//$res .=	"package \"<&browser> ".$this->m_applicationName."\" {\n";

		//
		// for each component 
		//
		foreach ( $this->m_components as $component ) {
			$res .=	$component->getPlantUMLCode();
		}	

		//
		// end of main application
		//
		//$res .=	"}\n";

		//
		// render links between components
		//
		foreach ( $this->m_components as $component ) {

			$connectiontype_plantuml = '';
			if ($component->getConnectionType() !== '') {
				$connectiontype_plantuml = " : ".
				str_replace("(","\\n(",$component->getConnectionType());
			}
				 
			foreach ($component->getConnections() as $connectedComponent) {
				$res .= $component->getUUID().
					" ...> ".
					$connectedComponent->getUUID().
					$connectiontype_plantuml.
					"\n";
			}
                }

                //
                // add final stuff
                //

                return $res;

        }

}

abstract class ApplicationElement {

        // TODO I18N
        private $m_id            = 'no_id';
        private $m_label         = 'unlabeled';
        private $m_uid;

        public function getUUID(){
                if (!isset($this->m_uid)){
                        $this->m_uid = sprintf( 'UUID%04x%04x%04x%04x%04x%04x%04x%04x',
                                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                                mt_rand(0, 0x0fff) | 0x4000,
                                mt_rand(0, 0x3fff) | 0x8000,
                                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
			
                }

                return $this->m_uid;
        }
        public function getId() {
                return $this->m_id;
        }

        public function setId( $id ) {
                $this->m_id = $id;
        }

        public function getLabel() {
                return $this->m_label;
        }

        public function setLabel( $label ) {
                $this->m_label = preg_replace('/.*:/','',$label);
        }

}

/**
 * Class representing an application server 
 */
class ApplicationServer extends ApplicationElement {

        private $m_components	= array();

        public function getComponents() {
                return $this->m_components;
        }

        public function addComponent( $component ) {
                $this->m_components[] = $component;
        }

        public function getPlantUMLCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;
                // render server
		// end of server

		return "";
	}
		
}

/**
 * Class representing an application app 
 */
class ApplicationApp extends ApplicationElement {

        private $m_components	= array();

        public function getComponents() {
                return $this->m_components;
        }

        public function addComponent( $component ) {
                $this->m_components[] = $component;
        }

        public function getPlantUMLCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;
                // render app
		// end of server

		return "";
	}
		
}

/**
 * Class representing an application component 
 */
class ApplicationComponent extends ApplicationElement {

        private $m_application;				// reference to containing application
        private $m_componenttype 	= '';		// type of component
        private $m_connectiontype 	= '';		// type of connection to other components 

        private $m_servers		= array();      // servers hosting this component
	private $m_connections		= array();	// other components connected to this component
	private $m_apps			= array();	// apps that contain this component

        public function setComponentType( $type ) {
                $this->m_componenttype = $type;
        }

        public function getComponentType() {
                return $this->m_componenttype;
        }

        public function setConnectionType( $type ) {
                $this->m_connectiontype = $type;
        }

        public function getConnectionType() {
                return $this->m_connectiontype;
        }

        public function setApplication( $app ) {
                $this->m_application =  $app;
        }

        public function getApplication() {
                return $this->m_application;
	}

        public function addServer( $server ) {
                $this->m_servers[] = $server;
                $server->addComponent( $this );
        }

        public function getServers() {
                return $this->m_servers;
        }

        public function addApp( $app ) {
                $this->m_apps[] = $app;
                $app->addComponent( $this );
        }

        public function getApps() {
                return $this->m_apps;
        }

        public function addConnection( $component ) {
                $this->m_connections[] = $component;
        }

        public function getConnections() {
                return $this->m_connections;
        }

        public function getPlantUMLCode() {
                global $IP, $srfgPicturePath, $srfgIP, $wgArticlePath;

		// do we render apps ?
		$res = '';
		
		if (!empty($this->m_apps)) {
			$res .= "package \"";
			$UUID = "";
			foreach ($this->m_apps as $j => $app) {
				$link = preg_replace('/ /','_',$app->getId());
				$link = str_replace('$1',urlencode($link),$wgArticlePath);
				if ($j > 0) {
					$res .= ", ";
				}
				$res .= "<&browser> [[".$link." ".$app->getLabel()."]]";
				$UUID .= $app->getUUID();
			}
			$res .= "\" as ".$UUID." {\n";
		}
		

		// do we render servers ?
		if (!empty($this->m_servers)) {
			// render server or cluster of servers
			$res .= "frame \"";
			$UUID = "";
			foreach ($this->m_servers as $i => $server) {
			//	$res .= "debug -> $i\n";
				$link = preg_replace('/ /','_',$server->getId());
				$link = str_replace('$1',urlencode($link),$wgArticlePath);
				if ($i > 0) {
					$res .= ", ";
				}
				$res .= "<&monitor> [[".$link." ".$server->getLabel()."]]";
				$UUID .= $server->getUUID();
			}
			$res .= "\" as ".$UUID." {\n";
		}

		// of course we do render components !
		// render component
		$link = preg_replace('/ /','_',$this->getId());
		$link = str_replace('$1',urlencode($link),$wgArticlePath);
		$res .=	"component \"<&puzzle-piece> [[".$link." ".$this->getLabel()."]]\" as ".$this->getUUID()."\n";
		
		// do we have an end of servers ?
		if (!empty($this->m_servers)) {
			// end of server
			$res .=	"}\n";
		}

		// do we have an end of apps ?
		
		if (!empty($this->m_apps)) {
			// end of app
			$res .= "}\n";
		}


		return $res;
	}

}
