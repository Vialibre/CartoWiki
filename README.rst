CartoWiki
=========

Cartowiki extension to SemanticMediaWiki lets you create svg maps of different parts of your IT architecture :
 - Processes maps;
 - Business functions overview (POS);
 - Application functional coverage;
 - Technical application architecture;
 - Inter-application flows.

Install cartowiki extension only
--------------------------------
NB : Cartowiki extensions needs a mediawiki and an important set of extensions.

Please follow the *Full install* instructions in next paragraph before installing Cartowiki.

1. Clone CartoWiki extension to your ``extensions`` folder:

``git clone https://git.framasoft.org/Vialibre/CartoWiki.git``
  
2. Add the following line to your ``LocalSettings.php`` :

``require_once( "$IP/extensions/CartoWiki/CartoWiki.php" );``
  
3. Visit your wiki's ``Special:Version`` page to check the installation.


Full install with all the dependancies
--------------------------------------

These instructions are for Debian Jessie or Wheezy.

1. Install a lamp server :

``apt-get install apache2 mysql-server php5 php5-mysql php5-intl``

2. Additional packages are needed to get the sources

- with git :

  ``apt-get install git curl``
  
- with composer for php dependancies :

  ``curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer``

3. Get Mediawiki from git :

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/core.git -b REL1_25 /var/www/cartowiki``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/vendor.git -b REL1_25 /var/www/cartowiki/vendor``

``git clone https://gerrit.wikimedia.org/r/mediawiki/skins/Vector -b REL1_25 /var/www/cartowiki/skins/vector``

``chown -R www-data: /var/www/cartowiki/images``

4. Finish install with the web install, or use the following for command line equivalent :

``cd /var/www/cartowiki``

``php maintenance/install.php --server "http://cartowiki.ens-cachan.fr/cartowiki" --dbserver localhost --dbtype mysql --lang fr --confpath '/var/www/cartowiki' --dbname mediawiki --dbpass password --dbuser mediawiki --scriptpath '/cartowiki' --pass admin_cartowiki "Cartographie de l'ENS" admin``

Your admin account will be ``admin:cartowiki_admin``

5. Install the following extensions from REL1_25 branch of mediawiki official git repos :

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/Arrays.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/CategoryTree.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/ExternalData.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/HeaderTabs.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/ImageMap.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/Loops.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/MassEditRegex.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/PageSchemas.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/ParserFunctions.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/Variables.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/DataTransfer.git``

6. Add some packages for the graphic extensions :

``apt-get install graphviz imagemagick graphviz graphviz-dev``

7. PlantUML is the UML renderer, it needs java and a jar file :

``apt-get install openjdk-7-jre-headless``

``cd /var/www/cartowiki``

``git clone https://github.com/pjkersten/PlantUML.git``

``cd /var/www/cartowiki/PlantUML``

``wget https://downloads.sourceforge.net/project/plantuml/plantuml.jar``

8. The following extensions are from the community :

``git clone https://github.com/p12tic/NativeSvgHandler.git``

``git clone -b REL1_28 https://github.com/mediawiki4intranet/RegexParserFunctions.git``

9. The semantic extensions :

``composer require mediawiki/semantic-media-wiki ~2.2``

``composer require mediawiki/semantic-result-formats ~2.2``

``composer require mediawiki/semantic-forms ~3.3``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticFormsInputs.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticCompoundQueries.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticDrilldown.git``

``git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/SemanticInternalObjects.git``

10. The additional skins :

``cd /var/www/cartowiki/styles/``

``git clone https://github.com/thingles/foreground.git``
    
11. To enable the extensions, add the following lines to your ``/var/www/cartowiki/LocalSettings.php`` :

::

  //
  // Settings for cartowiki
  //
  // enable
  require_once("$IP/extensions/CartoWiki/CartoWiki.php");
  // namespaces
  $wgExtraNamespaces[3000] = "Application";
  $wgExtraNamespaces[3001] = "Application_Discussion";   // underscore required
  $wgContentNamespaces[] = 3000;
  $smwgNamespacesWithSemanticLinks[3000] = true;
  $wgExtraNamespaces[3002] = "Composant_logiciel";
  $wgExtraNamespaces[3003] = "Composant_logiciel_Discussion";   // underscore required
  $wgContentNamespaces[] = 3002;
  $smwgNamespacesWithSemanticLinks[3002] = true;
  $wgExtraNamespaces[3004] = "Matériel";
  $wgExtraNamespaces[3005] = "Matériel_Discussion";   // underscore required
  $wgContentNamespaces[] = 3004;
  $smwgNamespacesWithSemanticLinks[3004] = true;
  $wgExtraNamespaces[3006] = "Personne";
  $wgExtraNamespaces[3007] = "Personne_Discussion";   // underscore required
  $wgContentNamespaces[] = 3006;
  $smwgNamespacesWithSemanticLinks[3006] = true;
  $wgExtraNamespaces[3008] = "Processus";
  $wgExtraNamespaces[3009] = "Processus_Discussion";   // underscore required
  $wgContentNamespaces[] = 3008;
  $smwgNamespacesWithSemanticLinks[3008] = true;
  $wgExtraNamespaces[3010] = "Flux";
  $wgExtraNamespaces[3011] = "Flux_Discussion";   // underscore required
  $wgContentNamespaces[] = 3010;
  $smwgNamespacesWithSemanticLinks[3010] = true;
  $wgExtraNamespaces[3012] = "Salle_informatique";
  $wgExtraNamespaces[3013] = "Salle_informatique_Discussion";   // underscore required
  $wgContentNamespaces[] = 3012;
  $smwgNamespacesWithSemanticLinks[3012] = true;
  $wgExtraNamespaces[3014] = "Baie";
  $wgExtraNamespaces[3015] = "Baie_Discussion";   // underscore required
  $wgContentNamespaces[] = 3014;
  $smwgNamespacesWithSemanticLinks[3014] = true;
  $wgExtraNamespaces[3016] = "Structure";
  $wgExtraNamespaces[3017] = "Structure_Discussion";   // underscore required
  $wgContentNamespaces[] = 3016;
  $smwgNamespacesWithSemanticLinks[3016] = true;
  $wgExtraNamespaces[3018] = "Métier";
  $wgExtraNamespaces[3019] = "Métier_Discussion";   // underscore required
  $wgContentNamespaces[] = 3018;
  $smwgNamespacesWithSemanticLinks[3018] = true;
  $wgExtraNamespaces[3020] = "Objectif";
  $wgExtraNamespaces[3021] = "Objectif_Discussion";   // underscore required
  $wgContentNamespaces[] = 3020;
  $smwgNamespacesWithSemanticLinks[3020] = true;
  $wgExtraNamespaces[3022] = "Donnée";
  $wgExtraNamespaces[3023] = "Donnée_Discussion";   // underscore required
  $wgContentNamespaces[] = 3022;
  $smwgNamespacesWithSemanticLinks[3022] = true;
  $wgExtraNamespaces[3024] = "Lien_réseau";
  $wgExtraNamespaces[3025] = "Lien_réseau_discussion";   // underscore required
  $wgContentNamespaces[] = 3024;
  $smwgNamespacesWithSemanticLinks[3024] = true;
  //
  // Common settings for mediawiki
  //
  $wgLanguageCode = "fr";
  $wgUseAjax = true;
  $wgEnableUploads = true; # Enable uploads
  $wgPhpCli = false;
  // enable massedit
  require_once ( "$IP/extensions/MassEditRegex/MassEditRegex.php" );
  $wgGroupPermissions['sysop']['masseditregex'] = true; // Allow administrators to use Special:MassEditRegex
  // set debug mode
  // uncomment the following lines to enable
  $wgDebugLogFile = "/var/log/mediawiki/debug-{$wgDBname}.log";
  $wgDebugToolbar = true;
  $wgShowExceptionDetails = true;
  $wgDevelopmentWarnings = true;
  //
  // Settings for graphic extensions
  //
  // image map
  require_once("$IP/extensions/ImageMap/ImageMap.php");
  // plantuml
  require_once("$IP/extensions/PlantUML/PlantUML.php");
  $plantumlImagetype = 'svg';
  // native SVG support
  require_once "$IP/extensions/NativeSvgHandler/NativeSvgHandler.php";
  $wgNativeSvgHandlerEnableLinks = true; //Set to false to disable links over SVG images
  $wgFileExtensions[] = 'svg';
  $wgSVGConverter = 'ImageMagick';
  //
  // Settings for semantic extensions
  //
  $wgLanguageCode = "fr";
  // enable
  enableSemantics( parse_url( $wgServer, PHP_URL_HOST ) );
  require_once("$IP/extensions/SemanticCompoundQueries/SemanticCompoundQueries.php");
  require_once("$IP/extensions/SemanticDrilldown/SemanticDrilldown.php");
  require_once("$IP/extensions/SemanticFormsInputs/SemanticFormsInputs.php");
  require_once("$IP/extensions/SemanticInternalObjects/SemanticInternalObjects.php");
  // drilldown settings
  $sdgNumResultsPerPage=500;
  $sdgHideCategoriesByDefault = true;
  $sdgMinValuesForComboBox=0;
  //
  // Skin settings
  //
  // choose foreground skin 
  require_once( "$IP/skins/foreground/foreground.php" );
  $wgDefaultSkin = "foreground";
  $wgForegroundFeatures = array(
  'showActionsForAnon' => true,
  'NavWrapperType' => 'divonly',
  'showHelpUnderTools' => true,
  'showRecentChangesUnderTools' => true,
  'wikiName' => &$GLOBALS['wgSitename'],
  'navbarIcon' => true,
  'IeEdgeCode' => 1,
  'showFooterIcons' => 0,
  'addThisFollowPUBID' => ''
  );
  // edit tabs extension
  require_once( "$IP/extensions/HeaderTabs/HeaderTabs.php" );
  $sfgRenameEditTabs = true;
  //
  // Settings for syntax extensions
  //
  // enable
  require_once("$IP/extensions/Arrays/Arrays.php");
  require_once("$IP/extensions/CategoryTree/CategoryTree.php");
  require_once("$IP/extensions/ExternalData/ExternalData.php");
  require_once "$IP/extensions/DataTransfer/DataTransfer.php";
  require_once("$IP/extensions/Loops/Loops.php");
  require_once("$IP/extensions/PageSchemas/PageSchemas.php");
  require_once("$IP/extensions/ParserFunctions/ParserFunctions.php");
  require_once("$IP/extensions/RegexParserFunctions/RegexParserFunctions.php");
  require_once("$IP/extensions/Variables/Variables.php");
  $wgPFEnableStringFunctions = true;

Initial import (structure)
--------------------------

A dump is provided here : `dump-nodata.xml
<https://git.framasoft.org/Vialibre/CartoWiki/blob/master/dumps/dump-nodata.xml>`_.

``cd /var/www/cartowiki``

``php maintenance/importDump.php /var/lib/cartowiki/dumps/dump-nodata.xml > /var/lib/cartowiki/importdump.log``

``php maintenance/rebuildrecentchanges.php > /var/lib/cartowiki/rebuildrecentchanges.log``

FAQ
---

What should I do if a map or schema doesn't show or is not updated on a page ?
  You can try to connect as admin user, and select « Actions > Refresh » in the local menu.

What should I do if some category pages (e.g. «domaines et fonctionnalités») do not update ?
  It might take some time for some information to get updated after the initial import.
  You can force the update this way :

  - connect to the mediawiki as user ``admin``
  - go to page ``Special:SMWAdmin``
  - click on ``begin data update`` (ou ``Commencer la mise à jour des données``)

What should I do if some pages still don't update ?
  If the above is not enough for some category pages, just editing them and
  saving them again helps sometimes trigger the update for the page and all the connected ones.