<?php
/**
 * CartoWiki
 *
 * @file
 * @ingroup Extensions
 * @author Vialibre <jocelyn@colibriste.net>
 * @license GNU General Public Licence 2.0 or later
 */

$wgExtensionCredits['semantic'][] = array(
	'path' => __FILE__,
	'name' => 'CartoWiki',
	'author' => array(
		'Jocelyn Viallon',
	),
	'version'  => '0.0.1',
	'url' => '',
	'description' => 'Formats additionnels d’affichage pour la cartographie de l’urbanisation des SI.',
	'license-name'	=> 'GPL-2.0+'
);

/* Setup */

$dir = dirname( __FILE__ );
$formats = $dir . '/formats';

// Register files
$wgAutoloadClasses['SRFApplication'] = $formats . '/SRF_Application.php';
$wgAutoloadClasses['SRFAppFlow'] = $formats . '/SRF_AppFlow.php';
$wgAutoloadClasses['SRFPOSMap'] = $formats . '/SRF_POS.php';
$wgAutoloadClasses['SRFUMLProcess'] = $formats . '/SRF_UMLProcess.php';

// Register new formats to SemanticResultFormat
$smwgResultFormats['application'] = 'SRFApplication';
$smwgResultFormats['appflow'] = 'SRFAppFlow';
$smwgResultFormats['pos2'] = 'SRFPOSMap';
$smwgResultFormats['umlprocess'] = 'SRFUMLProcess';

// Activate the formats
$srfgFormats[] = 'application';
$srfgFormats[] = 'appflow';
$srfgFormats[] = 'pos2';
$srfgFormats[] = 'network';
$srfgFormats[] = 'umlprocess';

// Change the logo
$wgLogo =  "$dir/images/Logo.png";
